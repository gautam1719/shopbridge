import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { IProduct } from './interface/product';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ProductServiceService } from './services/product-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProductsComponent implements OnInit {

  constructor(private dataService: ProductServiceService, private router: Router) { }

  displayedColumns = [ 'Name', 'Description', 'Price', 'Image' , 'delete'];
  products: IProduct[] = [];
  dataSource = new MatTableDataSource(this.products);
  filterValue: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, {}) sort: MatSort;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnInit() {
    this.getProducts();
  }

  onNavigate(productCode) {
    this.router.navigateByUrl('product/details/' + productCode);
  }
  
  filterProduct(value: string):void{
    // this.serviceAPI.getDataByFilter(value).subscribe(response =>
    // {
    // this.dataSource= response[‘products’];
    // //Set page index
    // //Enable disable next button here
    // //this.pageSize
    // });
  }

  delete(value: number) {
    this.dataService.deleteProduct(value).subscribe((data: any)=>{
      if(data && data.Id > 0) {
        alert('successfully deleted');
        this.getProducts();
      }
    })  
  }

  getProducts() {
    this.dataService.getProducts().subscribe((data: IProduct[])=>{
      console.log(data);
      this.products = data;
      this.dataSource.data = this.products;
      this.dataSource.paginator = this.paginator;
    })  
  }
}
