import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { IProduct } from '../interface/product';

@Injectable({
  providedIn: 'root'
})
export class ProductServiceService {
  private REST_API_SERVER = environment.apiUrl;

  constructor(private httpClient: HttpClient) { }
  
  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
  
  public getProducts(): Observable<IProduct[]> {
    return this.httpClient.get<IProduct[]>(this.REST_API_SERVER + 'shops/getproducts').pipe(retry(2),catchError(this.handleError));
  }

  public deleteProduct(id: number): Observable<any> {
    return this.httpClient.get<any>(this.REST_API_SERVER + 'shops/removeproduct/'+ id).pipe(retry(2),catchError(this.handleError));
  }
  public AddProduct(product: any): Observable<IProduct> {
    return this.httpClient.post<IProduct>(this.REST_API_SERVER + 'shops/addproduct', product).pipe(retry(2),catchError(this.handleError));
  }
}