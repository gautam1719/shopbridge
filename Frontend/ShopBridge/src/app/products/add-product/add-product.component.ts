import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProductServiceService } from '../services/product-service.service';
import * as _ from 'lodash';
import { IProduct } from '../interface/product';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddProductComponent implements OnInit {
  addProductForm: FormGroup;  //declaring our form variable
  imageError: string;
    isImageSaved: boolean;
    cardImageBase64: string;

  constructor(private dataService: ProductServiceService) { }

  ngOnInit(): void {
    this.addProductForm = new FormGroup({
      product_name: new FormControl(null),
      product_description: new FormControl(null),
      product_image: new FormControl(null),
      product_price: new FormControl(null),
    });
  }

  onSubmit() {
    console.log(this.addProductForm);
    let obj = this.CreateProductObject(this.addProductForm.value);
    obj.ImagePath = this.cardImageBase64;
    this.dataService.AddProduct(obj).subscribe((data: IProduct) => {
      if (data && data.Id > 0) {
        alert('successfully added');
        this.addProductForm.reset();
        this.removeImage();
      }
    })
  }

  CreateProductObject(data: any) {
    let product = {
      Name: data.product_name,
      Description: data.product_description,
      ImagePath: data.product_image,
      Price: data.product_price,
    }
    return product;
  }

  fileChangeEvent(fileInput: any) {
    this.imageError = null;
    if (fileInput.target.files && fileInput.target.files[0]) {
        // Size Filter Bytes
        const max_size = 20971520;
        const allowed_types = ['image/png', 'image/jpeg'];
        const max_height = 15200;
        const max_width = 25600;

        if (fileInput.target.files[0].size > max_size) {
            this.imageError =
                'Maximum size allowed is ' + max_size / 1000 + 'Mb';

            return false;
        }

        if (!_.includes(allowed_types, fileInput.target.files[0].type)) {
            this.imageError = 'Only Images are allowed ( JPG | PNG )';
            return false;
        }
        const reader = new FileReader();
        reader.onload = (e: any) => {
            const image = new Image();
            image.src = e.target.result;
            image.onload = rs => {
                const img_height = rs.currentTarget['height'];
                const img_width = rs.currentTarget['width'];

                console.log(img_height, img_width);


                if (img_height > max_height && img_width > max_width) {
                    this.imageError =
                        'Maximum dimentions allowed ' +
                        max_height +
                        '*' +
                        max_width +
                        'px';
                    return false;
                } else {
                    const imgBase64Path = e.target.result;
                    this.cardImageBase64 = imgBase64Path;
                    this.isImageSaved = true;
                    // this.previewImagePath = imgBase64Path;
                }
            };
        };

        reader.readAsDataURL(fileInput.target.files[0]);
    }
}

removeImage() {
    this.cardImageBase64 = null;
    this.isImageSaved = false;
}

}
