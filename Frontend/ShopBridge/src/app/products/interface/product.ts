export interface IProduct {
    Id: number | null;
    Name: string;
    Description: string;
    Price?: number;
    ImagePath?: string;
}

export class Product {
    Id: number | null;
    Name: string;
    Description: string;
    Price?: number;
    ImagePath?: string;
}