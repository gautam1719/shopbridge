import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ProductsComponent } from './products/products.component';
import { AddProductComponent } from './products/add-product/add-product.component';
import { HomeComponent } from './home/home.component';
import { ViewProductComponent } from './products/view-product/view-product.component';

const routes: Routes = [
  { path: "products", component: ProductsComponent },
  { path: "addproduct", component: AddProductComponent },
  { path: "product/details/:id", component: ViewProductComponent },
  { path: 'home', component: HomeComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes), MatFormFieldModule, MatInputModule],
  exports: [RouterModule]
})

export class AppRoutingModule { }
