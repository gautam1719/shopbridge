﻿using Shops.Context;
using Shops.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Unity;

namespace Shops.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ShopServiceContext Db;

        private readonly DbSet<T> dbSet;

        public Repository()
        {
            Db = new ShopServiceContext();
            dbSet = Db.Set<T>();
        }
        public async Task<IEnumerable<T>> GetAll()
        {
            return await dbSet.ToListAsync();
        }

        public async Task<T> GetByIdAsync(object Id)
        {
            return await dbSet.FindAsync(Id);
        }

        public void Insert(T obj)
        {
            dbSet.Add(obj);
        }
        public void Update(T obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
        }
        public void Delete(T obj)
        {
            dbSet.Remove(obj);
        }
        public void Save()
        {
            Db.SaveChanges();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.Db != null)
                {
                    this.Db.Dispose();
                    this.Db = null;
                }
            }
        }


    }
}