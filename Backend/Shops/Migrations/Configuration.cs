namespace Shops.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Shops.Context.ShopServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Shops.Context.ShopServiceContext context)
        {
            //context.Products.AddOrUpdate(x => x.Id,
            //    new Product() { Id = 1, Name = "Jane Austen" },
            //    new Product() { Id = 2, Name = "Charles Dickens" },
            //    new Product() { Id = 3, Name = "Miguel de Cervantes" }
            //    );
        }
    }
}
