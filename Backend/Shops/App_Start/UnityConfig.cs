using Microsoft.Practices.ServiceLocation;
using AutoMapper;
using Shops.Models;
using Shops.Repository;
using Unity;
using Unity.WebApi;
using Shops.Context;
using System.Web.Mvc;
using System.Web.Http;

namespace Shops
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers
            container.RegisterType<IRepository<Product>, Repository<Product>>();

            var config = new MapperConfiguration(cfg =>
            {
                //Create all maps here
                cfg.AddProfile<MapperProfile>();
            });
            IMapper mapper = config.CreateMapper();
            container.RegisterInstance(mapper);

            //DependencyResolver.SetResolver(new UnityDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);
        }
    }
}